/**
 * @file       cbuffer.h
 * @copyright
 * @license
 * @version    1.0.0
 * @date
 * @author     Triet Luu
 * @brief      Circular Buffer
 *             This Circular Buffer is safe to use in IRQ with single reader,
 *             single writer. No need to disable any IRQ.
 *
 *             Capacity = <size> - 1
 * @note       None
 * @example    None
 */
/* Define to prevent recursive inclusion ------------------------------ */
#ifndef __CBUFFER_H
#define __CBUFFER_H

/* Includes ----------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
//#include "cmsis_compiler.h"

/* Public defines ----------------------------------------------------- */
#define CB_MAX_SIZE (0x00800000)

/* Public enumerate/structure ----------------------------------------- */
typedef struct
{
    uint8_t *data;
    uint32_t size;
    uint32_t writer;
    uint32_t reader;
    uint32_t overflow;
    bool active;
} cbuffer_t;

/* Public macros ------------------------------------------------------ */
/* Public variables --------------------------------------------------- */
/* Public function prototypes ----------------------------------------- */
void cb_init(cbuffer_t *cb, void *buf, uint32_t size){
    cb->data=(uint8_t*)buf;
    cb->active=true;
    cb->size= size;
    cb->reader=0;
    cb->writer=0;
}

//=============== Clear buffer===========================//
void cb_clear(cbuffer_t *cb){
    for(uint8_t i=0;i<cb->size;i++){
        *(cb->data+i)=0;
    }
    cb->writer=0;
    cb->overflow=0;
    cb->reader=0;
}

//=============== Read data===========================//
uint32_t cb_read(cbuffer_t *cb, void *buf, uint32_t nbytes){
    printf("\nDataRead: ");
    for(uint32_t i=0;i<nbytes;i++){
        *((uint8_t*)buf+i)= *(cb->data+cb->reader);
        printf("%d\t",*((uint8_t*)buf+i));
        cb->reader++;

        //==No data for read==//
        if(cb->reader == cb->writer){
            printf("No data for read");
            break;
        }

        //==Move reader to 0 index in buffer==//
        if(cb->reader == cb->size){
            cb->reader=0;
        }
    }  
}

//=============== Write data===========================//
uint32_t cb_write(cbuffer_t *cb, void *buf, uint32_t nbytes){
    for(uint32_t i=0;i<nbytes;i++){
        *(cb->data+cb->writer)=*((uint8_t*)buf);
        (uint8_t*)buf++;
        cb->writer++;

        //==Move writer to 0 index in buffer==//
        if(cb->writer == cb->size){
            cb->writer=0;
            cb->overflow++;// count time overflow 
        }

        //== buffer full==//
        if(cb->writer == cb->reader){
            printf("\nFull Data");
            break;
        }
    }

    //== Print Data write==//
    printf("\nDataWrite : ");
    for(uint8_t i=0;i<cb->size;i++){
        printf("%d\t",*(cb->data+i));
    }

    //==Check overflow==//
    if(cb->overflow >1){
        printf("\nData Overflow: %d Byte",(((cb->overflow-1)*cb->size)+cb->writer));
    }
}

//=============== Count data===========================//
uint32_t cb_data_count(cbuffer_t *cb){
    if(cb->overflow >= 1){
        printf("\nData count:%d Byte (Full)",cb->size);
    }
    else{
        printf("\nData count:%d Byte",cb->writer);
    }
}

//=============== Count space===========================//
uint32_t cb_space_count(cbuffer_t *cb){
      if(cb->overflow == 0 ){
    printf("\nFree Space: %d Byte",cb->size-cb->writer);
        }
    else{
        printf("\nData is overflow");
        }
}

#endif // __CBUFFER_H

/* End of file -------------------------------------------------------- */
