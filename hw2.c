#include <stdio.h>
#include <stdint.h>
#include "cbuffer.h"
cbuffer_t cb;
uint8_t cb_buff[17];
uint8_t a[] = {1, 1, 2, 3, 4,5,6,7};
uint8_t c[] = {5, 6, 7,8 ,9,10,11,12,13,14,15,16,17,18,19,20,21};
uint8_t b[9];
uint8_t d[17];
//=====================
//==========
void main(){
    cb_init(&cb, cb_buff, 17);
    cb_write(&cb, a, 8);
    cb_read(&cb,b,9);
    cb_write(&cb, c, 17);
    cb_read(&cb,d,17);
    cb_data_count(&cb);
    cb_space_count(&cb);
    cb_clear(&cb);
    cb_data_count(&cb);
    return;}